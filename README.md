### 万能的新手引导
感谢 Creator星球开发社区  公众号

- GodGuide 文件夹是引导所有功能

#### 介绍
Cocos Creator 新手引导框架，支持浏览器自动引导，流程录制与回放，可实现自动化测试！

#### 使用说明

1. 演示工程为Creator 2.3.3版本
2. 引导框架兼容 2.x.x 所有版本，低版本demo 工程UI可能会有错乱请自行调整
3. 引导相关代码在assets/GodGuide目录下
4. Task1.js 为引导任务配置

### 入口脚本 LoadGuide.ts   
```javascript
    @property(cc.Prefab)
    PREFAB: cc.Prefab = null; //预制件 引导功能主 prefab (GodGuide.prefab)

    @property(cc.Node)
    parent: cc.Node = null;  //预制件实例化后所在的父节点
```
### 操作手册
点击录制流程 -> 点击要引导的按钮  完成后 -> 点击 流程回放 （网页后台会输出 你的点击记录  把相应数据 复制到  Task1.js 配置文件中  配置文件可以创建多个）

