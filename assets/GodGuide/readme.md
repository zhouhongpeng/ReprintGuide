
###GodGuide 引导
框架主要用到了 async  （异步加载） 
//eachSeries是一种async异步函数库中的一个函数，可以实现循环的异步操作。
//async.series({ ###串行 且无关联  串行执行方法

task1.ts  等 task*.js 是 引导任务配置文件  配置文件可以创建多个

### 配置文件详解
```javascript
    export const task = {
    name: '进入商店', //文件描述
    debug: true,    // 是否debug模式 可以控制log输出
    autorun: true,  // 是否自动播放
    steps: [    // 引导主要数据  可通过 制作流程 功能 获取到
        {
            desc: '文本提示',
            command: { cmd: 'text', args: ['欢迎体验Shawn的新手引导框架\n特别感谢【黝黑蜗科】提供的TS版本', '本案例演示：\n1.文本提示指令\n2.手指定位指令\n3.自动执行引导\n4.点击操作录像', '首先，请点击首页按钮'] },
        },
        {
            desc: "点击nodeUI/btn1",
            command: {
                cmd: "finger",
                args: "nodeUI/btn1"
            },
            delay: 1.491
        }, {
            desc: "点击nodeUI/btn3",
            command: {
                cmd: "finger",
                args: "nodeUI/btn3"
            },
            delay: 1.015
        }, {
            desc: "点击nodeUI/btn4",
            command: {
                cmd: "finger",
                args: "nodeUI/btn4"
            },
            delay: 0.918
        }, {
            desc: "点击nodeUI/btn2",
            command: {
                cmd: "finger",
                args: "nodeUI/btn2"
            },
            delay: 0.866
        }]
}
```

> 入口脚本 LoadGuide.ts
```javascript 
    @property(cc.Prefab)
    PREFAB: cc.Prefab = null; //预制件 引导功能主 prefab

    @property(cc.Node)
    parent: cc.Node = null;  //预制件实例化后所在的父节点
```

> 主引导功能脚本  GodGuide.ts  GodGuide.prefab
```javascript 
    //初始化引导  调用位置 LoadGuide.ts
    setTask(task) {
        if (this._task) {
            cc.warn('当前任务还未处理完毕！');
            return;
        }

        this._debugNode.active = !!task.debug;
        this._autorun.string = `自动执行(${task.autorun ? '开' : '关'})`;
        this._task = task;
    }

    // 运行 引导  调用位置 LoadGuide.ts
    run(callback?) {
        if (!this._task) {
            return;
        }
        console.log('this._task.steps---------->', this._task.steps)
        async.eachSeries(this._task.steps, (step, cb) => {
            this._processStep(step, cb);  //引导任务 调用逻辑
        }, () => {
            this._task = null;
            cc.log('任务结束');
            this._mask.node.active = false;
            if (this._finger) {
                this._finger.active = false;
            }

            if (callback) {
                callback();
            }
        });
    }
```