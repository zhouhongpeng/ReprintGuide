export const task = {
    name: '进入商店',
    debug: true,
    autorun: true,
    steps: [
        {
            desc: '文本提示',
            command: { cmd: 'text', args: ['欢迎体验Shawn的新手引导框架\n特别感谢【黝黑蜗科】提供的TS版本', '本案例演示：\n1.文本提示指令\n2.手指定位指令\n3.自动执行引导\n4.点击操作录像', '首先，请点击首页按钮'] },
        },
        {
            desc: "点击nodeUI/btn1",
            command: {
                cmd: "finger",
                args: "nodeUI/btn1"
            },
            delay: 1.491
        }, {
            desc: "点击nodeUI/btn3",
            command: {
                cmd: "finger",
                args: "nodeUI/btn3"
            },
            delay: 1.015
        }, {
            desc: "点击nodeUI/btn4",
            command: {
                cmd: "finger",
                args: "nodeUI/btn4"
            },
            delay: 0.918
        }, {
            desc: "点击nodeUI/btn2",
            command: {
                cmd: "finger",
                args: "nodeUI/btn2"
            },
            delay: 0.866
        }]
}